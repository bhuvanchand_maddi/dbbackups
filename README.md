# Database Backups Automation
> ## Introduction:
This project automates database backups for web reports test developments across three databases i.e **Microsoft SQL Server,Oracle Server and PostgreSQL** using software provisioning tool **Ansible**.

> ## Prerequisites:
**Ansible**,**pip** and **pywinrm** in Control node
**Python** in Managed node

> ## Project Setup:
Ansible needs to be installed in any redhat linux machine and it communicates with 3 database servers using *SSH* connection, the overview of setp looks like:

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggTFJcbiBzdWJncmFwaCBSZWRoYXRMaW51eE1hY2hpbmVcbiAgICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXVxuICAgIGVuZFxuICAgICBzdWJncmFwaCB3aW5kb3dzX3NlcnZlcnNcbiAgICBBW0Fuc2libGUgQ29udHJvbGxlcl0gLS0tLT58d2lucm18IERbKG1zc3Fsc2VydmVyKV1cbiAgICBBW0Fuc2libGUgQ29udHJvbGxlcl0gLS0tLT58d2lucm18IEVbXCJ3aW4yMDEycjIoRm9yIHN0b3JpbmcgYmFja3VwZmlsZXMpXCJdXG4gICAgZW5kXG4gICAgIHN1YmdyYXBoIHJlZGhhdF9saW51eFxuICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXSAtLS0tPnxTU0h8IEJbKHBvc3RncmVzcWwpXVxuICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXSAtLS0tPnxTU0h8IENbb3JhY2xlY2xpZW50XSA9PT5GWyhvcmFjbGVzZXJ2ZXIpXVxuICAgIGVuZFxuY2xhc3NEZWYgZGF0YWJhc2VzIGZpbGw6I2JiZixzdHJva2U6I2Y2NixzdHJva2Utd2lkdGg6MnB4LGNvbG9yOiNmZmY6IDUgNVxuY2xhc3MgRCxCLEYgZGF0YWJhc2VzOyAgICBcbiAgICBcblxuIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggTFJcbiBzdWJncmFwaCBSZWRoYXRMaW51eE1hY2hpbmVcbiAgICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXVxuICAgIGVuZFxuICAgICBzdWJncmFwaCB3aW5kb3dzX3NlcnZlcnNcbiAgICBBW0Fuc2libGUgQ29udHJvbGxlcl0gLS0tLT58d2lucm18IERbKG1zc3Fsc2VydmVyKV1cbiAgICBBW0Fuc2libGUgQ29udHJvbGxlcl0gLS0tLT58d2lucm18IEVbXCJ3aW4yMDEycjIoRm9yIHN0b3JpbmcgYmFja3VwZmlsZXMpXCJdXG4gICAgZW5kXG4gICAgIHN1YmdyYXBoIHJlZGhhdF9saW51eFxuICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXSAtLS0tPnxTU0h8IEJbKHBvc3RncmVzcWwpXVxuICAgIEFbQW5zaWJsZSBDb250cm9sbGVyXSAtLS0tPnxTU0h8IENbb3JhY2xlY2xpZW50XSA9PT5GWyhvcmFjbGVzZXJ2ZXIpXVxuICAgIGVuZFxuY2xhc3NEZWYgZGF0YWJhc2VzIGZpbGw6I2JiZixzdHJva2U6I2Y2NixzdHJva2Utd2lkdGg6MnB4LGNvbG9yOiNmZmY6IDUgNVxuY2xhc3MgRCxCLEYgZGF0YWJhc2VzOyAgICBcbiAgICBcblxuIiwibWVybWFpZCI6eyJ0aGVtZSI6ImRlZmF1bHQifSwidXBkYXRlRWRpdG9yIjpmYWxzZX0)

#### Installing Ansible(Control node):
The ansible control node should be a linux distribution, so ansible can be installed in any Redhat Linux machine.
**Steps to install:**
1. Install and enable EPEL repository configuration package using the following command.
>$yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

2.Install Ansible from EPEL repository
>$yum install -y ansible  

3.Install Pywinrm for managing windows nodes
>$pip install pywinrm

#### Redhat linux managed nodes setup:
Setup for redhat managed nodes are as follows:
**Postgresql:**
1.Create user in postgesql db machine, ansible control node ssh with this user.
>useradd pgdbbackupuser

2.Add user to sodoers.d group
>cd /etc/sudoers.d
touch pgdbbackupuser
nano pgdbbackupuser

Add below content and save:
>pgdbbackupuser ALL=(ALL) NOPASSWD: ALL

3.Establish SSH passwordless authentication from control node to postgresql db machine
- Connect to ansible control node
- Generate public and private keys using **ssh-keygen**
>ssh-keygen
- Copy the public key to **authorized_keys** folder of pgdbbackupuser in Postgesql db machine
>ssh-copy-id pgdbbackupuser@ip_of_db

**Oracle:**
1.Create user orcldbbackupuser in oracle client machine, ansible control node ssh with this user.
>useradd orcldbbackupuser

2.Add user to sodoers.d group
>cd /etc/sudoers.d
touch orcldbbackupuser
nano orcldbbackupuser

Add below content and save:
>orcldbbackupuser ALL=(ALL) NOPASSWD: ALL

3.Establish SSH passwordless authentication from control node to postgresql db machine
- Connect to ansible control node
- Generate public and private keys using **ssh-keygen**
>ssh-keygen
- Copy the public key to **authorized_keys** folder of orcldbbackupuser in oracle client machine
>ssh-copy-id orcldbbackupuser@ip_of_db

4.Verify **tnsnames.ora** file, oracle client should be connected to oracle server with service name **orcl**

#### Windows server managed nodes setup:
- Ansible uses **WinRM** protocol to establish a connection with Windows hosts
- It listens on HTTP Port 5985 and HTTPS Port 5986 for WinRM connectivity.
- There are two main components of the WinRM service that governs how Ansible can interface with the Windows host: the **listener** and the **service configuration settings**.
- This script([ConfigureRemotingForAnsible.ps1](https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1)) sets up both HTTP and HTTPS listeners with a self-signed certificate and enables the Basic authentication option on the service.
- We can create separate user using which ansible control node communicates with windows managed nodes over winrm, but i've used existing Administrator user.
- This setup is common for both MSSQLServer and win2012r2(which is used for storing backup results) machines.

#### Inventory file of Control node:
* The Ansible inventory file defines the hosts and groups of hosts upon which commands, modules, and tasks in a playbook operate.
* The file can be in one of many formats depending on your Ansible environment and plugins.
* The default location for the inventory file is /etc/ansible/hosts
* The inventory file can list individual hosts or user-defined groups of hosts
* In this project setup, ansible control node controls 3 databases and 1 windows server node and the inventory file looks like below

```
[windows_servers]
mssqlserver ansible_host=<ip_of_mssqlserver>
win2012r2 ansible_host=<ip_of_windowsserver>
[redhat_linux]
postgresql ansible_ssh_host=<ip_of_postgresql_db> ansible_user=pgdbbackupuser
oracleclient ansible_ssh_host=<ip_of_oracle_client> ansible_user=orcldbbackupuser
``` 

> ## Usuage Instructions:








